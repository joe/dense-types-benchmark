add_subdirectory("test")

#install headers
install(FILES
  register.hh
  sequencetools.hh
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/benchmark-template)
