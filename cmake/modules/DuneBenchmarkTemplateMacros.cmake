# File for module specific CMake tests.
find_package(Benchmark REQUIRED)
if(NOT BENCHMARK_FOUND)
  message(FATAL_ERROR "Google benchmark library is requiered; see https://github.com/google/benchmark")
endif(NOT BENCHMARK_FOUND)