# .. cmake_module::
#
#    Find the Google Benchmark library
#
#    See https://github.com/google/benchmark
#
#    See the CppCon 2015 talk by Chandler Carruth:
#    "Tuning C++: Benchmarks, and CPUs, and Compilers! Oh My!"
#    https://www.youtube.com/watch?v=nXaxk27zwlk
#
#    You may set the following variables to modify the
#    behaviour of this module:
#
#    :ref:`BENCHMARK_ROOT`
#       Path list to search for Google benchmark
#
#    Sets the following variables:
#
#    :code:`BENCHMARK_FOUND`
#       True if the Google benchmark library was found.
#
# .. cmake_variable:: BENCHMARK_ROOT
#
#   You may set this variable to have :ref:`FindBenchmark` look for the Google
#   benchmark package in the given path before inspecting system paths.
#


# search for location of header "benchmark/benchmark.h", only at positions given by the user
find_path(BENCHMARK_INCLUDE_DIR
  NAMES "benchmark/benchmark.h"
  PATHS ${BENCHMARK_PREFIX} ${BENCHMARK_ROOT}
  PATH_SUFFIXES include
  NO_DEFAULT_PATH)
# try default paths now
find_path(BENCHMARK_INCLUDE_DIR
  NAMES "benchmark/benchmark.h")

# look for library benchmark, only at positions given by the user
find_library(BENCHMARK_LIB benchmark
  PATHS ${BENCHMARK_PREFIX} ${BENCHMARK_ROOT}
  PATH_SUFFIXES lib lib64
  NO_DEFAULT_PATH
  DOC "Google benchmark library")
# try default paths now
find_library(BENCHMARK_LIB benchmark)

if(BENCHMARK_LIB)
  # needs the flags to make std::thread work
  set(BENCHMARK_LIB ${BENCHMARK_LIB} ${STDTHREAD_LINK_FLAGS})
endif(BENCHMARK_LIB)

# check if library works
if(BENCHMARK_LIB AND BENCHMARK_INCLUDE_DIR)
  include(CMakePushCheckState)
  cmake_push_check_state()

  include(CheckCXXSymbolExists)
  set(CMAKE_REQUIRED_INCLUDES ${CMAKE_REQUIRED_INCLUDES} ${BENCHMARK_INCLUDE_DIR})
  set(CMAKE_REQUIRED_LIBRARIES ${CMAKE_REQUIRED_LIBRARIES} ${BENCHMARK_LIB})
  check_cxx_symbol_exists(benchmark::Initialize benchmark/benchmark.h BENCHMARK_LIB_WORKS)

  cmake_pop_check_state()
endif(BENCHMARK_LIB AND BENCHMARK_INCLUDE_DIR)

# behave like a CMake module is supposed to behave
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  "benchmark"
  DEFAULT_MSG
  BENCHMARK_INCLUDE_DIR BENCHMARK_LIB BENCHMARK_LIB_WORKS
)

mark_as_advanced(BENCHMARK_INCLUDE_DIR BENCHMARK_LIB)

# if both headers and library are found, store results
if(BENCHMARK_FOUND)
  set(BENCHMARK_INCLUDE_DIRS ${BENCHMARK_INCLUDE_DIR})
  set(BENCHMARK_LIBRARIES ${BENCHMARK_LIB})
  # log result
  file(APPEND ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CMakeOutput.log
    "Determing location of Google benchmark succeeded:\n"
    "Include directory: ${BENCHMARK_INCLUDE_DIRS}\n"
    "Library directory: ${BENCHMARK_LIBRARIES}\n\n")
else(BENCHMARK_FOUND)
  # log errornous result
  file(APPEND ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CMakeError.log
    "Determing location of Google benchmark failed:\n"
    "Include directory: ${BENCHMARK_INCLUDE_DIR}\n"
    "Library directory: ${BENCHMARK_LIB}\n\n")
endif(BENCHMARK_FOUND)

# set HAVE_BENCHMARK for config.h
set(HAVE_BENCHMARK ${BENCHMARK_FOUND})

# register all BENCHMARK related flags
if(HAVE_BENCHMARK)
  dune_register_package_flags(LIBRARIES "${BENCHMARK_LIB}"
                              INCLUDE_DIRS "${BENCHMARK_INCLUDE_DIR}")
endif(HAVE_BENCHMARK)
