set(modules
  "DuneBenchmarkTemplateMacros.cmake"
  "FindBenchmark.cmake"
  )

install(FILES ${modules} DESTINATION ${DUNE_INSTALL_MODULEDIR})
