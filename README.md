CPU scaling
===========

If the library gives you the warning
```
***WARNING*** CPU scaling is enabled, the benchmark real time measurements may be noisy and will incur extra overhead.
```
set the CPU-freq governor `performance`.  I theory you should use `cpupower`
to do this, which supercedes `cpufreq-set` like this:
```sh
sudo cpupower --cpu all frequency-set -g performance
```

However, cpupower currently (2016-11-04) has a bug that prevents that from
working, see
[Debian BTS](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=843071) and
[kernel Bugzilla](https://bugzilla.kernel.org/show_bug.cgi?id=135391).  Until
that is fixed, you can use `bin/set-governor`, a wrapper around `cpufreq-set`
from this module to achieve the same:
```sh
sudo bin/set-governor performance
```
