// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

/** @file
 *
 * Example for a single benchmark.
 */

#include <config.h>

#include <iostream>

#include <benchmark/benchmark.h>

#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI


// Define Benchmark
static void BM_Norm(benchmark::State& state)
{
  Dune::FieldVector<double, 3> x = { 1.0, 2.0, 3.0 };
  // Tell the compiler that we might be writing the address of x somewhere
  // into global memory, and in addition might be changing x through that
  // address.  I.e. the compiler cannot know the value of x after this call,
  // and thus cannot statically optimize for certain values.
  benchmark::DoNotOptimize(x);

  while (state.KeepRunning())
  {
    // Tell the compiler that we are doing something with the result of
    // x.two_norm(), so he has to actually compute it.  Tell him also that we
    // are accessing global memory -- including, possibly, the address of x
    // that we got earlier.  This means the compiler cannot pull the
    // evaluation of x.two_norm() out of the loop because for all he knows, we
    // might be changing the value of x.  Unfortunately, it also means that
    // the compiler has to reload the value of x from memory in each
    // iteration, which is a bit more than we actually require.
    benchmark::DoNotOptimize(x.two_norm());
  }
}
// Register the function as a benchmark
BENCHMARK(BM_Norm);

int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);
  ::benchmark::Initialize(&argc, argv);
  if(argc > 1) {
    std::cerr << "Warning: " << *argv << ": unhandled arguments:";
    while(*++argv)
      std::cerr << " " << *argv;
    std::cerr << std::endl;
  }
  ::benchmark::RunSpecifiedBenchmarks();
}
