// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

/** @file
 *
 * Example showing a benchmark family parameterized by a template parameter,
 * and how to compute it's complexity.
 */

#include <config.h>

#include <cstddef>
#include <iostream>
#include <utility>

#include <benchmark/benchmark.h>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI

#include <dune/benchmark-template/register.hh>
#include <dune/benchmark-template/sequencetools.hh>

// benchmark m.solve(x, rhs)
struct Solve {
  template<std::size_t n>
  void operator()(benchmark::State &state,
                  std::integral_constant<std::size_t, n>) const
  {
    using benchmark::ClobberMemory;
    using benchmark::DoNotOptimize;

    using T = double;

    Dune::FieldMatrix<T, n, n> m(T(0));
    for(std::size_t i = 0; i < n; ++i)
      m[i][i] = i+1;
    // Compiler: you can't assume anything about the value of m after this.
    // In addition, the value of m might changes whenever memory is clobbered.
    DoNotOptimize(&m);

    Dune::FieldVector<T, n> rhs;
    for(std::size_t i = 0; i < n; ++i)
      rhs[i] = i;
    DoNotOptimize(&rhs);

    Dune::FieldVector<T, n> x(T(0));
    // Compiler: the address of x is known, someone might access or modify it's
    // values when you're not looking
    DoNotOptimize(&x);

    while (state.KeepRunning())
    {
      m.solve(x, rhs);
      // Compiler: you're not looking...  Make sure the value of x in memory
      // is correct.  Also, you need to reload the values of m and rhs from
      // memory after this, they might have changed.  (Unfortunately I know of
      // no way to force computation of x except forcing it to memory, and
      // reloading m and rhs from memory.)
      benchmark::ClobberMemory();
    }
    state.SetComplexityN(n*n);
  }
};

int main(int argc, char** argv)
{
  using Dune::BenchmarkTemplate::registerTemplate;
  using Dune::BenchmarkTemplate::Generator::imultiply;
  using Dune::BenchmarkTemplate::Sentinel::size;

  registerTemplate("solve", Solve(),
                   std::pair<imultiply<1, 2>, size<7> >())
    ->Complexity();

  Dune::MPIHelper::instance(argc, argv);

  ::benchmark::Initialize(&argc, argv);
  if(argc > 1) {
    std::cerr << "Warning: " << *argv << ": unhandled arguments:";
    while(*++argv)
      std::cerr << " " << *argv;
    std::cerr << std::endl;
  }
  ::benchmark::RunSpecifiedBenchmarks();
}
