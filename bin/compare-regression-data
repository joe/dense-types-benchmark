#!/usr/bin/env python3

import argparse
import contextlib
import csv
import itertools
import sys

from sys import argv

def goodness(value, limit=8):
    g = ""
    if value <= 0.95:
        chars = int(20*(1 - value))
        if chars > limit:
            g = "-"*(limit-3)+"..."
        else:
            g = "-"*chars
    if value >= 1.05:
        chars = int(20*(value - 1))
        if chars > limit:
            g = "+"*(limit-3)+"..."
        else:
            g = "+"*chars
    return g


def copyheader(infile, outfile, prefix):
    """Copy the non-csv header to outfile, prepending prefix.

    We recognize the start of the csv table by a line containing at least 3
    ",".

    Returns an interator that should be used in place of infile (we have to
    peek ahead in infile and actually read the first line of the csv table --
    this iterator first returns that line and then everything infile returns).
    """
    for line in infile:
        if line.count(',') >= 3:
            return itertools.chain([line], infile)
        else:
            outfile.write(prefix + line)
    # no csv data found -- return empty iterator
    return itertools.chain()


# stolen from itertools docs
def all_equal(iterable):
    "Returns True if all the elements are equal to each other"
    g = itertools.groupby(iterable)
    return next(g, True) and not next(g, False)


def zip_strict(*iterables):
    "like zip(), but assert that all iterables are of the same length"
    stopcounter = len(iterables)
    def sentinel():
        nonlocal stopcounter
        stopcounter -= 1
        yield None
    for values in zip(*[ itertools.chain(i, sentinel()) for i in iterables ]):
        assert stopcounter in [0, len(iterables)]
        if stopcounter == 0:
            return
        else:
            yield values


def merge_result(basefile, *followupfiles, outfile=sys.stdout, column="min"):
    
    #fileheader
    outfile.write("Comparing against '{}' using column '{}'\n"
                  .format(basefile.name, column))
    basefile  = copyheader(basefile, outfile,
                           "Baseline ({}): ".format(basefile.name))
    followupfiles = [
        copyheader(f, outfile, "Follow-up {} ({}): ".format(i, f.name))
        for i, f in zip(itertools.count(), followupfiles)
    ]

    # make csv handlers
    # put the baseline in the front, that simplifies matters
    readers = [ csv.reader(basefile) ] + [ csv.reader(f) for f in followupfiles ]
    writer = csv.writer(outfile, dialect='unix')

    # tableheader
    heads        = [ next(reader)            for reader in readers ]
    name_indices = [ head.index("name")      for head   in heads   ]
    unit_indices = [ head.index("time_unit") for head   in heads   ]
    time_indices = [ head.index(column)      for head   in heads   ]

    row = ["name", "time_unit", "baseline"]
    for i in range(len(readers)-1):
        row.extend(tmpl.format(i) for tmpl in [ "time_{}", "speedup_{}", "goodness_{}"])
    writer.writerow(row)

    for rows in zip_strict(*readers):
        assert all_equal([ rows[i][name_indices[i]] for i in range(len(rows)) ])
        name = rows[0][name_indices[0]]

        assert all_equal([ rows[i][unit_indices[i]] for i in range(len(rows)) ])
        unit = rows[0][unit_indices[0]]

        base_str, *val_strs = [ rows[i][time_indices[i]] for i in range(len(rows)) ]

        row = [name, unit, base_str]

        base_val = float(base_str)
        for val_str in val_strs:
            speedup = base_val/float(val_str)
            row.extend([ val_str, "{:5.2f}".format(speedup), goodness(speedup) ])

        writer.writerow(row)


# main

parser = argparse.ArgumentParser(
    description="Compare CSV data from regression testing",
    epilog="""Output is written to stdout.  '-' may appear at most once as an argument
              and means read from stdin.  The files should be produced by
              Google's benchmark library: header lines that are not part of
              the table are copied through with a prefix.  The start of the
              table is recognized by a line that contains at least three
              commas.""")
parser.add_argument("baseline", metavar="BASELINE", type=argparse.FileType('r'),
                    help="CSV data file with the baseline data.")
parser.add_argument("followup", metavar="FOLLOWUP", nargs='*', default=[sys.stdin],
                    type=argparse.FileType('r'),
                    help="CSV data files with followup data (default: '-')")
parser.add_argument("-c", "--column", default="min",
                    help="Name of column to compare (default: 'min').")
args = parser.parse_args()

assert ([args.baseline] + args.followup).count(sys.stdin) <= 1

merge_result(args.baseline, *args.followup, column=args.column)
