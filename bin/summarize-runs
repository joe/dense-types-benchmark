#!/usr/bin/env python3

import argparse
import csv
import itertools
import statistics
import sys

def badness(typical, worst, limit=8):
    """Graphically display the badness of deviations.

    `typical' should be a some typical deviation, i.e. the median or the
    standard deviation, `worst` should be something like the maximum
    deviation.  Both should be relative and non-negative.  `worst' is usually
    larger than `typical`, but that is not required.

    For every 0.05 of deviation, we produce a character: '*' for `typical',
    '-' for `worst'.  `typical' characters overlay the beginning of the
    `worst' characters.  If the resulting string would be langer than `limit',
    it is truncated and an ellipsis is appended such that the end result has
    exactly length `limit'.

    """
    assert typical >= 0
    assert worst >= 0

    typical_char = "*"
    worst_char   = "-"
    ellipsis = "..."

    chars = int(20*typical)
    if chars > limit:
        return typical_char*(limit-len(ellipsis))+ellipsis
    else:
        msg = typical_char*chars

    chars = int(20*worst)
    if chars <= limit:
        return msg + worst_char*(chars - len(msg))

    # there will be "..." at the end
    if len(msg) + len(ellipsis) >= limit-3:
        return msg[:limit-len(ellipsis)]+ellipsis
    else:
        return msg + worst_char*(limit - len(ellipsis) - len(msg)) + ellipsis
    


# stolen from itertools docs
def all_equal(iterable):
    "Returns True if all the elements are equal to each other"
    g = itertools.groupby(iterable)
    return next(g, True) and not next(g, False)


def copyheader(infile, outfile, prefix):
    """Copy the non-csv header to outfile, prepending prefix.

    We recognize the start of the csv table by a line containing at least 3
    ",".

    Returns an interator that should be used in place of infile (we have to
    peek ahead in infile and actually read the first line of the csv table --
    this iterator first returns that line and then everything infile returns).
    """
    for line in infile:
        if line.count(',') >= 3:
            return itertools.chain([line], infile)
        else:
            outfile.write(prefix + line)
    # no csv data found -- return empty iterator
    return itertools.chain()


def zip_strict(*iterables):
    "like zip(), but assert that all iterables are of the same length"
    stopcounter = len(iterables)
    def sentinel():
        nonlocal stopcounter
        stopcounter -= 1
        yield None
    for values in zip(*[ itertools.chain(i, sentinel()) for i in iterables ]):
        assert stopcounter in [0, len(iterables)]
        if stopcounter == 0:
            return
        else:
            yield values


class min_summarizer:
    "Summarize by the minimum value, determining badness by median and max"

    def name(self):
        return "min"
    
    def headers(self):
        return [ "min", "median_pct", "max_pct", "badness" ]
    
    def summary(self, value_strings):
        result = []

        min_val = min(value_strings, key=float)
        result.append(min_val)
        min_val = float(min_val)

        median_val = statistics.median(map(float, value_strings))
        median_rel = (median_val - min_val)/min_val
        result.append("{:5.2f}".format(median_rel*100))

        max_val = max(map(float, value_strings))
        max_rel = (max_val - min_val)/min_val
        result.append("{:5.2f}".format(max_rel*100))

        result.append(badness( median_rel, max_rel ))

        return result
        

class mean_summarizer:
    """Summarize by the mean value, determining badness by sample standard
    deviation and maximum deviation
    """

    def name(self):
        return "mean"
    
    def headers(self):
        return [ "mean", "stddev_pct", "max_pct", "badness" ]

    def summary(self, value_strings):
        result = []
        values = list(map(float, value_strings))

        mean_val = statistics.mean(values)
        result.append("{:6g}".format(mean_val))

        stddev_val = statistics.stdev(values, mean_val)
        stddev_rel = stddev_val/mean_val
        result.append("{:5.2f}".format(stddev_rel*100))

        max_val = max(abs(val - mean_val) for val in values)
        max_rel = max_val/mean_val
        result.append("{:5.2f}".format(max_rel*100))

        result.append(badness( stddev_rel, max_rel ))

        return result
        

def combine(infiles, column, summarizer, outfile):
    # fileheader
    outfile.write("Summarize on '{}' using '{}'\n".format(column, summarizer.name()))
    infiles = [ copyheader(i, outfile, "{}: ".format(i.name)) for i in infiles ]

    # make csv handlers
    readers = [ csv.reader(i) for i in infiles ]
    writer = csv.writer(outfile, dialect='unix')

    # tableheader
    # we don't require the column names of all files to be identical, but we
    # do require the columns 'name', 'time_unit' and at least one of
    # 'real_time' or 'cpu_time'.  The name and the time_unit of a row must
    # match for all input files.
    heads        = [ next(reader)            for reader in readers ]
    name_indices = [ head.index("name")      for head   in heads   ]
    unit_indices = [ head.index("time_unit") for head   in heads   ]
    time_indices = [ head.index(column)      for head   in heads   ]

    writer.writerow( [ "name", "time_unit" ] + summarizer.headers() )

    for rows in zip_strict(*readers):
        assert all_equal([ rows[i][name_indices[i]] for i in range(len(rows)) ])
        name = rows[0][name_indices[0]]

        assert all_equal([ rows[i][unit_indices[i]] for i in range(len(rows)) ])
        unit = rows[0][unit_indices[0]]

        values = [ rows[i][time_indices[i]] for i in range(len(rows)) ]
        
        row = [ name, unit ] + summarizer.summary(values)

        writer.writerow(row)


# main

parser = argparse.ArgumentParser(
    description="Summarize multiple runs from performance regression testing",
    epilog="Output is written to stdout.")
parser.add_argument("infiles", metavar="INFILE", nargs='*', default=sys.stdin,
                    type=argparse.FileType('r'),
                    help="""CSV data files with the benchmark data; '-' may appear at most once and
                    means read from stdin.  The files should be produced by
                    Google's benchmark library: header lines that are not part
                    of the table are copied through with a prefix.  The start
                    of the table is recognized by a line that contains at
                    least three commas.""")
parser.add_argument("-c", "--column", default="real_time", action="append",
                    help="Name of column to summarize (default: 'real_time').")
parser.add_argument("--min", dest='summarizer', default=min_summarizer,
                    action="store_const", const=min_summarizer,
                    help="""Summarize by the minimum value, determining badness by median and max
                    (default).""")
parser.add_argument("--mean", dest='summarizer',
                    action="store_const", const=mean_summarizer,
                    help="""Summarize by the mean value, determining badness by sample standard
                    deviation and maximum deviation.""")
args = parser.parse_args()

assert args.infiles.count(sys.stdin) <= 1

combine(args.infiles, args.column, args.summarizer(), sys.stdout)
